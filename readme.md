## Mutable neural network

[Access Paper Here](https://papers.ssrn.com/sol3/papers.cfm?abstract_id=4594278)

### Usage
```python3 experiment.py --iteration [int] --add_gap [int]```

### Parameters
-  **--iteration** : number of iterations to run training for.
-  **--add_gap** : training interval at which new node will be added to the mutable neural network. 

### Results
Folder ```results``` consists of output for accuracies and other parameters including addition gap. The file is saved in the form of ```json``` with name ```result_[--iteration]_[--add_gap].json```

Notebooks ```Log_plot.ipynb``` and ```plotting_same_iter_diff_addgap.ipynb``` can be used for plotting values from resulting json files.

<hr/>

Base Code credits:https://www.kaggle.com/code/wwsalmon/simple-mnist-nn-from-scratch-numpy-no-tf-keras/notebook 