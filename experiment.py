############################
## Libraries
############################
import numpy as np
from keras.datasets import mnist #Only for dataset
from collections import deque
import copy
import json
import argparse

np.random.seed(42)

############################
## Argument Parser
############################
parser = argparse.ArgumentParser(description='Process some integers.')
parser.add_argument('--iteration',
                    type=int,
                    help='Mention number of iterations')

parser.add_argument('--add_gap',
                    type=int,
                    help='Mention addition interval')

args = parser.parse_args()
############################
## Helper Functions (Common)
############################
def init_params(input_dim = 784, initial_size = 3):
    W1 = np.random.rand(initial_size, input_dim) - 0.5
    b1 = np.random.rand(initial_size, 1) - 0.5
    W2 = np.random.rand(10, initial_size) - 0.5
    b2 = np.random.rand(10, 1) - 0.5
    return W1, b1, W2, b2

def ReLU(Z):
    return np.maximum(Z, 0)

def softmax(Z):
    A = np.exp(Z) / sum(np.exp(Z))
    return A
    
def forward_prop(W1, b1, W2, b2, X):
    Z1 = W1.dot(X) + b1
    
    A1 = ReLU(Z1)
    Z2 = W2.dot(A1) + b2
    A2 = softmax(Z2)
    return Z1, A1, Z2, A2

def ReLU_deriv(Z):
    return Z > 0

def one_hot(Y):
    one_hot_Y = np.zeros((Y.size, Y.max() + 1))
    one_hot_Y[np.arange(Y.size), Y] = 1
    one_hot_Y = one_hot_Y.T
    return one_hot_Y

def backward_prop(Z1, A1, Z2, A2, W1, W2, X, Y):
    one_hot_Y = one_hot(Y)
    dZ2 = A2 - one_hot_Y
    dW2 = 1 / m * dZ2.dot(A1.T)
    db2 = 1 / m * np.sum(dZ2)
    dZ1 = W2.T.dot(dZ2) * ReLU_deriv(Z1)
    dW1 = 1 / m * dZ1.dot(X.T)
    db1 = 1 / m * np.sum(dZ1)
    return dW1, db1, dW2, db2

def update_params(W1, b1, W2, b2, dW1, db1, dW2, db2, alpha):
    W1 = W1 - alpha * dW1
    b1 = b1 - alpha * db1    
    W2 = W2 - alpha * dW2  
    b2 = b2 - alpha * db2    
    return W1, b1, W2, b2

def get_predictions(A2):
    return np.argmax(A2, 0)

def get_accuracy(predictions, Y):
    return np.sum(predictions == Y) / Y.size

def make_predictions(X, W1, b1, W2, b2):
    _, _, _, A2 = forward_prop(W1, b1, W2, b2, X)
    predictions = get_predictions(A2)
    return predictions

############################
## Helper Functions (Expanding NN)
############################
def update_new_node_weight(new_node, alpha, w_old, w_new):
    term = np.asarray([np.sum( np.square(w_old[:, i] - w_new[:, i] ))/w_old.shape[0] for i in range(w_old.shape[1])])
    term = np.sqrt(term)
    term = np.exp(term)/sum(np.exp(term))
        
    return (new_node + term)/2

def update_new_node_bias(new_bias, alpha, b_old, b_new):
    return (new_bias + np.mean(np.sqrt( np.square( b_old-b_new ) )))/2


def gradient_descent_mutable(X, Y, alpha, iterations, gap = 10, add_gap = 20, layer_limit = 10):
    train_accuracies = []
    test_accuracies = []
    W1, b1, W2, b2 = init_params()
    w_new = np.zeros(W1.shape[1])#np.random.rand((W1.shape[0]))
    b_new = np.zeros(1)    
    for i in range(iterations):
        # print(i)
        Z1, A1, Z2, A2 = forward_prop(W1, b1, W2, b2, X)
        dW1, db1, dW2, db2 = backward_prop(Z1, A1, Z2, A2, W1, W2, X, Y)
        
        W1_old, b1_old, W2_old =  copy.deepcopy(W1), copy.deepcopy(b1), copy.deepcopy(W2)
        W1, b1, W2, b2 = update_params(W1, b1, W2, b2, dW1, db1, dW2, db2, alpha)

        if i % gap == 0:
            # print("Iteration: ", i)
            train_predictions = get_predictions(A2)
            train_accuracies.append(get_accuracy(train_predictions, Y))
            test_predictions = make_predictions(X_test, W1, b1, W2, b2)
            test_accuracies.append(get_accuracy(test_predictions, Y_test))

        
        
        if W1.shape[0] < layer_limit:
            #Adding to original network
            update_w = update_new_node_weight(w_new, alpha, W1_old, W1)
            new_bias =  update_new_node_bias(b_new, alpha, b1, b1_old)
            if i%add_gap==0 :
                W1 = np.append(W1, [update_w], axis = 0)
                b1 = np.append(b1, [new_bias], axis = 0)
                W2 = np.append(W2.T, [np.random.rand(W2.shape[0])], axis = 0).T
                    # print(W1.shape, b1.shape, W2.shape)
        
    return W1, b1, W2, b2, train_accuracies, test_accuracies

############################
## Helper Functions (Classic NN)
############################
def gradient_descent(X, Y, alpha, iterations, gap = 1):
    train_accuracies = []
    test_accuracies = []

    W1, b1, W2, b2 = init_params(initial_size=10)
    for i in range(iterations):
        Z1, A1, Z2, A2 = forward_prop(W1, b1, W2, b2, X)
        dW1, db1, dW2, db2 = backward_prop(Z1, A1, Z2, A2, W1, W2, X, Y)
        W1, b1, W2, b2 = update_params(W1, b1, W2, b2, dW1, db1, dW2, db2, alpha)
        if i % gap == 0:
            train_predictions = get_predictions(A2)
            train_accuracies.append(get_accuracy(train_predictions, Y))
            test_predictions = make_predictions(X_test, W1, b1, W2, b2)
            test_accuracies.append(get_accuracy(test_predictions, Y_test))
    return W1, b1, W2, b2, train_accuracies, test_accuracies


############################
## Driver Function 
############################

if __name__ == "__main__":

    iterations = args.iteration
    add_gap = args.add_gap

    ############################
    ## Preparing Data
    ############################

    (X_train, Y_train), (X_test, Y_test) = mnist.load_data()
    m = X_train.shape[0]
    X_train = X_train.reshape(X_train.shape[0], X_train.shape[1]*X_train.shape[-1]).T
    X_train = X_train/255.

    X_test = X_test.reshape(X_test.shape[0], X_test.shape[1]*X_test.shape[-1]).T
    X_test = X_test/255.
    print("Dataset shape: ", X_train.shape, X_test.shape)

    print("Done loading data")

    ############################
    ## Training
    ############################
    W1_mut, b1_mut, W2_mut, b2_mut, train_acc_mutable, test_acc_mutable = gradient_descent_mutable(X_train,
                                                            Y_train,
                                                            alpha = 0.10,
                                                            iterations = iterations,
                                                            gap = 1,
                                                            layer_limit = 10,
                                                            add_gap=add_gap)


    print("Done training Mutable Net")


    W1, b1, W2, b2, train_acc_non_mutable, test_acc_non_mutable = gradient_descent(X_train,
                                    Y_train,
                                    alpha = 0.10,
                                    iterations = iterations,
                                    gap = 1)


    print("Done training Non-Mutable Net")

    

    ############################
    ## Writing to JSON File
    ############################

    json_data = json.dumps( {"train_acc_non_mutable": train_acc_non_mutable,
                            "test_acc_non_mutable": test_acc_non_mutable,
                            "train_acc_mutable": train_acc_mutable,
                            "test_acc_mutable": test_acc_mutable,
                            "iteration": iterations,
                            "add_gap": add_gap,
                            }, indent=4 )

    with open("results/result_{}_{}.json".format(iterations, add_gap), "w+") as output:
        output.write(json_data)
    print("done for iterations:{}, add_gap:{}".format(iterations, add_gap))

